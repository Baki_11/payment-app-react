const bodyParser = require('body-parser')
const cors = require('cors')
const express = require('express')

const app = express()

const mockData = {
  accountNumbers: ['75267', '58692', '20001', '55363']
}

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.get('/', (req, res) => {
  res.json(mockData)
})

app.get('/finalize', (req, res) => {
  setTimeout(() => res.json({ success: true }), 2000)
})

app.post('/', (req, res) => {
  res.json(req.body)
})

app.listen(4000, function () {
  console.log('Mock app listening on port 4000!')
})
