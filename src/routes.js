import React  from 'react';
import {Route, IndexRoute} from 'react-router';

import Navigator from './navigator/Navigator'
import Main from './navigator/main/Main'
import TakeCredit from './navigator/takeCredit/TakeCredit'
import TakeCreditStepOne from './navigator/takeCredit/takeCreditStepOne/TakeCreditStepOne'
import TakeCreditStepTwo from './navigator/takeCredit/takeCreditStepTwo/TakeCreditStepTwo'
import TakeCreditStepThree from './navigator/takeCredit/takeCreditStepThree/TakeCreditStepThree'

export default (
    <Route path="/" component={Navigator}>
        <IndexRoute component={Main}></IndexRoute>
        <Route path="/take-credit/step-one" component={TakeCredit}>
            <IndexRoute component={TakeCreditStepOne}></IndexRoute>
            <Route path="/take-credit/step-two" component={TakeCreditStepTwo}></Route>
            <Route path="/take-credit/step-three" component={TakeCreditStepThree}></Route>
        </Route>
    </Route>
)
