import { connect } from 'react-redux'
import React, { Component } from 'react'

//TODO: import from root T_T'
import userActions from '../../store/user/userActions'

import Payment from './payment/Payment'
import Summary from './summary/Summary'

const PAYMENT = 'Payment'
const SUMMARY = 'Summary'

class Main extends Component {
    constructor(props) {
        super(props)

        this.state = {
            currentPage: PAYMENT,
        }
    }

    componentDidMount() {
        this.props.fetchAccountNumbers()
    }

    handleAcceptPayment() {
        this.setState({
            currentPage: SUMMARY
        })
    }

    handleInputChange(event) {
        this.props.handleInputChange(event.target.name, event.target.value)
    }

    handleSelectChange(event) {
        this.props.handleSelectChange(event.target.name, event.target.value)
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-xs-12">
                        {/* TODO: this should be done using a router */}
                        {this.state.currentPage === PAYMENT ? (
                            <Payment
                                user={this.props.user}
                                onAcceptPayment={() => this.handleAcceptPayment()}
                                onInputChange={this.handleInputChange.bind(this)}
                                onSelectChange={this.handleSelectChange.bind(this)}
                            />) : null}
                        {this.state.currentPage === SUMMARY ? (
                            <Summary
                                transferTo={this.props.user.transferTo}
                            />) : null}
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
      user: state.user
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
      handleInputChange: (field, value) => dispatch(userActions.userChangeTransferTo(field, value)),
      handleSelectChange: (field, value) => dispatch(userActions.userChangeBaseValue(field, value)),
      fetchAccountNumbers: () => dispatch(userActions.fetchAccountNumbers())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Main)
