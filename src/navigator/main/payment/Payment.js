import React from 'react'

function Payment(props) {
    return (
        <form>
            <div className="row">
                <div className="col-xs-12">
                    <div>Saldo {props.user.balance}</div>
                    <div>Account &nbsp;
                        <select
                            name="accountNo"
                            value={props.user.accountNo}
                            onChange={props.onSelectChange}>
                            { props.user.accountNoList.map(account => <option value={account} key={account}>{account}</option>) }
                        </select>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-xs-12"><hr /></div>
            </div>
            <div className="row">
                <div className="col-xs-4">Imię</div>
                <div className="col-xs-4">
                    <input type="text"
                        name="name"
                        value={props.user.transferTo.name}
                        onChange={props.onInputChange}/>
                </div>
            </div>
            <div className="row">
                <div className="col-xs-4">Nazwisko</div>
                <div className="col-xs-4">
                    <input type="text"
                        name="surname"
                        value={props.user.transferTo.surname}
                        onChange={props.onInputChange}/>
                </div>
            </div>
            <div className="row">
                <div className="col-xs-4">Adress</div>
                <div className="col-xs-4">
                    <input type="text"
                        name="address"
                        value={props.user.transferTo.address}
                        onChange={props.onInputChange}/>
                </div>
            </div>
            <div className="row">
                <div className="col-xs-4">Kwota</div>
                <div className="col-xs-4">
                    <input type="number"
                        name="amount"
                        value={props.user.transferTo.amount}
                        onChange={props.onInputChange}/>
                </div>
            </div>
            <button onClick={() => props.onAcceptPayment()}>ZATWIERŹ</button>
        </form>
    )
}

export default Payment
