import React from 'react'

function Summary(props) {
    return (
        <div>
            <div className="row">
                <div className="col-xs-4">Name</div>
                <div className="col-xs-4">{props.transferTo.name}</div>
            </div>
            <div className="row">
                <div className="col-xs-4">Surname</div>
                <div className="col-xs-4">{props.transferTo.surname}</div>
            </div>
            <div className="row">
                <div className="col-xs-4">Address</div>
                <div className="col-xs-4">{props.transferTo.address}</div>
            </div>
            <div className="row">
                <div className="col-xs-4">Amount</div>
                <div className="col-xs-4">{props.transferTo.amount}</div>
            </div>
        </div>
    )
}

export default Summary
