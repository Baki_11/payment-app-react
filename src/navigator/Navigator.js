import { Link } from 'react-router'
import React from 'react'

function Navigator(props) {
    return (
        <div className="container">
            <div className="row">
                <div className="col-xs-12">
                    <button type="button"><Link to="/">Make a Payment</Link></button>
                    <button type="button"><Link to="/take-credit/step-one">Take Credit</Link></button>
                </div>
            </div>
            <div className="row">
                <div className="col-xs-12">
                    {props.children}
                </div>
            </div>
        </div>
    )
}

export default Navigator
