import { connect } from 'react-redux'
import { Link } from 'react-router'
import Axios from 'axios'
import React, { Component } from 'react'

import userActions from '../../../store/user/userActions'

const BALANCE = 'balance'

class TakeCreditStepTwo extends Component {
    constructor() {
        super()

        this.state = {
            isLoading: false,
            creditFinalized: false
        }
    }

    finalizeCredit() {
        const API_URL = 'http://localhost:4000/finalize'
        this.setState({ isLoading: true })
        Axios.get(API_URL)
            .then(response => {
                this.setState({
                    isLoading: false,
                    creditFinalized: response.data.success
                })
                const newBalance = parseInt(this.props.user.balance) + parseInt(this.props.credit.amount)
                this.props.handleDiscountChange(BALANCE, newBalance)
            })
            .catch(error => {
                throw(error)
            })
    }

    render() {
        return (
            <div>
                <div className="row">
                    <div className="col-xs-12">
                        Summary, aka. are you sure?
                    </div>
                </div>

                <div className="row">
                    <div className="col-xs-6">
                        To account:
                    </div>
                    <div className="col-xs-6">
                        {this.props.user.accountNo}
                    </div>
                </div>

                <div className="row">
                    <div className="col-xs-6">
                        Amount
                    </div>
                    <div className="col-xs-6">
                        {this.props.credit.amount}
                    </div>
                </div>

                <div className="row">
                    <div className="col-xs-6">
                        Period (in months)
                    </div>
                    <div className="col-xs-6">
                        {this.props.credit.period}
                    </div>
                </div>

                <div className="row">
                    <div className="col-xs-6">
                        Interest Rate
                    </div>
                    <div className="col-xs-6">
                        {this.props.credit.interestRate}
                    </div>
                </div>

                <div>Current Step: 2/3 {this.state.creditFinalized}</div>

                {!this.state.isLoading && !this.state.creditFinalized ?
                    (<button type="button" onClick={this.finalizeCredit.bind(this)}>Finalize</button>) : null
                }

                {this.state.isLoading && !this.state.creditFinalized ? (<div>LOADING...</div>) : null }

                {this.state.creditFinalized ? (<button><Link to="/take-credit/step-three">Next Step</Link></button>) : null}
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        credit: state.credit,
        user: state.user
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
      handleDiscountChange: (field, value) => dispatch(userActions.userChangeBaseValue(field, value))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(TakeCreditStepTwo)
