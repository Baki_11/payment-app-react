import { connect } from 'react-redux'
import { Link } from 'react-router'
import React, { Component } from 'react'

//TODO: import from root T_T'
import userActions from '../../../store/user/userActions'
import creditActions from '../../../store/credit/creditActions'

const DISCOUNT = 'discount'
const INTEREST_RATE = 'interestRate'

class TakeCreditStepOne extends Component {
    componentDidMount() {
        this.props.fetchAccountNumbers()
    }

    handleSelectChange(event) {
        this.props.handleSelectChange(event.target.name, event.target.value)
    }

    handleDiscountChange(event) {
        const newDiscounts = Object.assign({}, this.props.credit.discounts, { [event.target.name]: event.target.checked })
        const newDiscount = calculateDiscount(newDiscounts)
        const newInterestRate = calculateInterestRate(this.props.credit.amount, this.props.credit.period, newDiscount)
        this.props.handleInputChange(INTEREST_RATE, newInterestRate)
        this.props.handleInputChange(DISCOUNT, newDiscount)
        this.props.handleDiscountChange(event.target.name, event.target.checked)
    }

    handleInputChange(event) {
        const credit = Object.assign({}, this.props.credit, { [event.target.name]: event.target.value })
        const newInterestRate = calculateInterestRate(credit.amount, credit.period, credit.discount)
        this.props.handleInputChange(INTEREST_RATE, newInterestRate)
        this.props.handleInputChange(event.target.name, event.target.value)
    }

    render() {
        return (
            <form>
                <div className="row">
                    <div className="col-xs-6">
                        To which account?
                    </div>
                    <div className="col-xs-6">
                        <select
                            name="accountNo"
                            value={this.props.user.accountNo}
                            onChange={this.handleSelectChange.bind(this)}>
                            { this.props.user.accountNoList.map(account => <option value={account} key={account}>{account}</option>) }
                        </select>
                    </div>
                </div>
                <div className="row">
                    <div className="col-xs-6">
                        Amount
                    </div>
                    <div className="col-xs-6">
                        <input type="text"
                                name="amount"
                                value={this.props.credit.amount}
                                onChange={this.handleInputChange.bind(this)}/>
                    </div>
                </div>

                <div className="row">
                    <div className="col-xs-6">
                        Period (in months)
                    </div>
                    <div className="col-xs-6">
                        <input type="text"
                                name="period"
                                value={this.props.credit.period}
                                onChange={this.handleInputChange.bind(this)}/>
                    </div>
                </div>

                <div className="row">
                    <div className="col-xs-6">
                        Interest Rate
                    </div>
                    <div className="col-xs-6">{this.props.credit.interestRate} %</div>
                </div>

                <div className="row">
                    <div className="col-xs-6">
                        Insure your house
                    </div>
                    <div className="col-xs-6">
                        <input type="checkbox"
                            name="house"
                            value={this.props.credit.discounts.house}
                            onChange={this.handleDiscountChange.bind(this)} />
                    </div>
                </div>

                <div className="row">
                    <div className="col-xs-6">
                        Insure your car
                    </div>
                    <div className="col-xs-6">
                        <input type="checkbox"
                            name="car"
                            value={this.props.credit.discounts.car}
                            onChange={this.handleDiscountChange.bind(this)} />
                    </div>
                </div>

                <div className="row">
                    <div className="col-xs-12">
                        <div>Current Step: 1/3</div>
                        <button type="button"><Link to="/take-credit/step-two">Next Step</Link></button>
                    </div>
                </div>
            </form>
        )
    }
}

function calculateDiscount(discounts) {
    return Object.keys(discounts).reduce((accumulator, item, index, arr) => {
        accumulator.push(discounts[item])
        return accumulator.filter(accumulatorItem => accumulatorItem)
    }, []).length * 14
}

function calculateInterestRate(amount, period, discount) {
    const interestBase = (amount / 1000 + period / 12) / 2
    const interestAfterDiscount = interestBase - interestBase * discount / 100
    if (interestAfterDiscount < 1) {
        return 1
    }
    return interestAfterDiscount > 10 ? 10 : interestAfterDiscount.toFixed(2)
}

const mapStateToProps = (state) => {
    return {
        credit: state.credit,
        user: state.user
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
      fetchAccountNumbers: () => dispatch(userActions.fetchAccountNumbers()),
      handleDiscountChange: (field, value) => dispatch(creditActions.creditChangeDiscountValue(field, value)),
      handleInputChange: (field, value) => dispatch(creditActions.creditChangeBaseValue(field, value)),
      handleSelectChange: (field, value) => dispatch(userActions.userChangeBaseValue(field, value))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(TakeCreditStepOne)

