import React from 'react'

function TakeCredit (props) {
    return (
        <div>
            Take credit
            {props.children}
        </div>
    )
}

export default TakeCredit
