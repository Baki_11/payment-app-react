import {
    CREDIT_CHANGE_BASE_VALUE,
    CREDIT_CHANGE_DISCOUNT_VALUE
} from './creditActionTypes'

const initialState = {
    amount: 0,
    interestRate: 0,
    period: 0,
    discount: 0,
    discounts: {},
    accountNo: '44444'
}

export function creditReducer(state = initialState, action) {
    switch (action.type) {
    case CREDIT_CHANGE_BASE_VALUE:
        return Object.assign({}, state, { [action.payload.field]: action.payload.value } )
    case CREDIT_CHANGE_DISCOUNT_VALUE:
        return Object.assign({}, state, {
            discounts: Object.assign({}, state.discounts, { [action.payload.field]: action.payload.value })
        })
    default:
        return state
    }
}
