import {
    CREDIT_CHANGE_BASE_VALUE,
    CREDIT_CHANGE_DISCOUNT_VALUE
} from './creditActionTypes'

function creditChangeBaseValue(field, value) {
    return {
        type: CREDIT_CHANGE_BASE_VALUE,
        payload: { field, value }
    }
}

function creditChangeDiscountValue(field, value) {
    return {
        type: CREDIT_CHANGE_DISCOUNT_VALUE,
        payload: { field, value }
    }
}

const creditAcions = { creditChangeBaseValue, creditChangeDiscountValue }

export default creditAcions
