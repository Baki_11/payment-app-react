import { combineReducers, createStore, compose, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk';

import { userReducer } from './user/userReducer'
import { creditReducer } from './credit/creditReducer'

const reducers = combineReducers({
    credit: creditReducer,
    user: userReducer
})

const middewares = [
    thunkMiddleware,
]

function store () {
    return createStore(reducers, compose(
        applyMiddleware(...middewares),
        window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
    ))
}

export default store
