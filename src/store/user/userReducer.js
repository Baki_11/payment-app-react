import {
    USER_CHANGE_ACCOUNT_NUMBERS,
    USER_CHANGE_BASE_VALUE,
    USER_CHANGE_TRANSFER_TO
} from './userActionTypes'

const initialState = {
    balance: 301,
    accountNoList: [],
    accountNo: '44444',
    transferTo: {
        name: '',
        surname: '',
        address: '',
        amount: 0
    }
}

export function userReducer(state = initialState, action) {
    switch (action.type) {
    case USER_CHANGE_TRANSFER_TO:
        return Object.assign({}, state, {
            transferTo: Object.assign({}, state.transferTo, { [action.payload.field]: action.payload.value } )
        })
    case USER_CHANGE_BASE_VALUE:
        return Object.assign({}, state, { [action.payload.field]: action.payload.value } )
    case USER_CHANGE_ACCOUNT_NUMBERS:
        return Object.assign({}, state, { accountNoList: action.payload })
    default:
        return state
    }
}
