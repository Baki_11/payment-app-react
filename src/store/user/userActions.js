import Axios from 'axios'

import {
    USER_CHANGE_ACCOUNT_NUMBERS,
    USER_CHANGE_BASE_VALUE,
    USER_CHANGE_TRANSFER_TO
} from './userActionTypes'

function userChangeTransferTo(field, value) {
    return {
        type: USER_CHANGE_TRANSFER_TO,
        payload: { field, value }
    }
}

function userChangeBaseValue(field, value) {
    return {
        type: USER_CHANGE_BASE_VALUE,
        payload: { field, value }
    }
}

function fetchAccountNumbers() {
    const API_URL = 'http://localhost:4000'
    return dispatch => {
        return Axios.get(API_URL)
            .then(response => {
                dispatch({ type: USER_CHANGE_ACCOUNT_NUMBERS, payload: response.data.accountNumbers })
            })
            .catch(error => {
                throw(error)
            });
    }
}

const userActions = { userChangeTransferTo, userChangeBaseValue, fetchAccountNumbers }

export default userActions
